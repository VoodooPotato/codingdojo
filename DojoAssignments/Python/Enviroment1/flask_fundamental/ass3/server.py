from flask import Flask,render_template,redirect,session
app = Flask(__name__)
app.secret_key = "Secret"

@app.route('/')
def root():
	try:
		session['counter'] += 1
	except: 
		session['counter']  = 0
	return render_template('counter.html')
@app.route('/count2')
def count2():
	try:
		session['counter'] += 1
	except:
		session['counter'] = 0
	return redirect('/')
@app.route('/reset')
def reset():
	session['counter'] = 0
	return redirect('/')

app.run(debug=True)