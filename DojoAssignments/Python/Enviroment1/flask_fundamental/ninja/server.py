from flask import Flask,render_template,redirect
app = Flask(__name__)
@app.route('/')
def root():
	return render_template('ninja.html', select = 'none')
@app.route('/ninja')
def ninja():
	return render_template('ninja.html',select = 'all')
@app.route('/ninja/<color>')
def colorninja(color):
	if(color == 'blue' or color =='orange' or color == 'purple' or color == 'red'):
		return render_template('ninja.html',select = color)
	else:
		return render_template('ninja.html',select = 'girl')
app.run(debug=True)