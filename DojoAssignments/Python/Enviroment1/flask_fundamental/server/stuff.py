from flask import Flask,render_template
app = Flask(__name__)

@app.route('/<vararg>')
def handler_function(vararg):
  # here you can use the variable "vararg"
  # if you want to see what our argument looks like
  print vararg
  # we could do other things with this information from this point on such as:
  # use it to retrieve some records from the database
  # render a particular template
  return render_template('index.html')
app.run(debug=True)