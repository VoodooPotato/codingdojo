# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render,redirect
from random import randrange

# Create your views here.
def root(request):
	print "root"
	return render(request,"land/index.html")
def enviroment(request,nums):
	print nums
	nums = int(nums)
	if(is_prime(nums)):
		print "it is prime"
		nums = randrange(1,50)
		print "new nums {}".format(nums) 
	if(nums <= 10):
		print "snow"
		return render(request, "land/picture.html", {'src':'static/land/snow.jpg'})
	elif(nums > 10 and nums <= 20):
		print "desert"
		return render(request, "land/picture.html", {'src': 'static/land/desert.jpg'})
	elif(nums > 20 and nums <= 30):
		print "forest"
		return render(request, "land/picture.html", {'src':'static/land/forest.jpg'})
	elif(nums > 30 and nums <= 40):
		print "vine"
		return render(request, "land/picture.html", {'src':'static/land/vineyard.jpg'})
	elif(nums > 40 and nums <= 50):
		print "landscape"
		return render(request, "land/picture.html", {'src':'static/land/landscape.jpg'})
	else:
		return redirect('/')

def is_prime(a):
    return all(a % i for i in xrange(2, a))

