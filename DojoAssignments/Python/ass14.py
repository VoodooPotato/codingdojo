def ass14(students):
	for student in students:
		print student['first_name']+" "+student['last_name']

def ass14b(users):

	for index,student in enumerate(users['Students']):
		print "{} - {} {} - {}".format(str(index + 1),student['first_name'],student['last_name'],str(len(student['first_name']) + len(student['last_name'])))
	for index,teacher in enumerate(users['Instructors']):
		print "{} - {} {} - {}".format(str(index + 1),teacher['first_name'],teacher['last_name'],str(len(student['first_name']) + len(student['last_name'])))
users = {
 'Students': [
     {'first_name':  'Michael', 'last_name' : 'Jordan'},
     {'first_name' : 'John', 'last_name' : 'Rosales'},
     {'first_name' : 'Mark', 'last_name' : 'Guillen'},
     {'first_name' : 'KB', 'last_name' : 'Tonel'}
  ],
 'Instructors': [
     {'first_name' : 'Michael', 'last_name' : 'Choi'},
     {'first_name' : 'Martin', 'last_name' : 'Puryear'}
  ]
 }

ass14b(users)