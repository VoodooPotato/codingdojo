def ass4(x):
	y = type(x);
	if(y == int):
		if(x > 100):
			print"That's a big number"
		else:
			print "That's a small number"
	elif(y == str):
		if(len(x)>50):
			print "Long sentence"
		else:
			print "Short sentence"
	else:
		if(len(x)>= 10):
			print "Big list!"
		else:
			print "Short list"


test = [
	45,
	100,
	455,
	0,
	-23,
	"Rubber baby buggy bumpers",
	"Experience is simply the name we give our mistakes",
	"Tell me and I forget. Teach me and I remember. Involve me and I learn.",
	"",
	[1,7,4,21],
	[3,5,7,34,3,2,113,65,8,89],
	[4,34,22,68,9,13,3,5,7,9,2,12,45,923],
	[],
	['name','address','phone number','social security number']]
for element in test:
	ass4(element)