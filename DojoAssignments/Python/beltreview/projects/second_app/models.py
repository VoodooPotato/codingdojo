# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from projects.first_app.models import Users

# Create your models here.

class Authors(models.Model):
	name = models.CharField(max_length = 40)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now_add=True)
	def __str__(self):
		return self.name

class Books(models.Model):
	title = models.CharField(max_length = 40)
	reviews = models.TextField()
	star = models.IntegerField()
	author = models.ForeignKey(Authors, on_delete=models.CASCADE)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return "{} by {}".format(self.title, self.author)

