# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class book(models.Model):
	title = models.CharField(max_length = 40)
	author = models.CharField(max_length = 40)
	published_date = models.DateTimeField()
	category = models.CharField(max_length = 10)

	
