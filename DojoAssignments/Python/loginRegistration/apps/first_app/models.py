# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
import re

class UserManager(models.Manager):
	def registerVal(self, postData):
		result = {'status':True,'errors':[]}
		user = []
		if not postData['f_name'] or len(postData['f_name']) < 3:
			result['status'] = False
			result['errors'].append('First name must be 3 chars long')
		if not postData['l_name'] or len(postData['l_name']) < 3:
			result['status'] = False
			result['errors'].append('Last name must be 3 chars long')
		if not postData['email'] or len(postData['email']) < 4 or not re.match(r'[^@]+@[^@]+\.[^@]+', postData['email']):
			result['status'] = False
			result['errors'].append('email must be valid')
		if not postData['password'] or len(postData['password']) < 8 or postData['password']!= postData['c_password']:
			result['status'] = False
			result['errors'].append('password must be valid')
		print result,'********************************'
		if result['status'] == True:
			user = Users.objects.filter(email=postData['email'])
		if len(user) != 0:
			result['status'] = False
			result['errors'].append('User already exists. Please try another email.')
		return result

	def loginVal(self,postData):
		result = {'status' : False, 'errors': [],'user': None}
		if not postData['email'] or len(postData['email']) < 4 or not re.match(r'[^@]+@[^@]+\.[^@]+', postData['email']):
			result['status'] = False
			result['errors'].append('what kind of email is this?')
		else:
			user = Users.objects.filter(email=postData['email'])
			if len(user) == 0:
				result['status'] = False
				result['errors'].append('There is no user with this email')
			elif len(postData['password']) < 5 or postData['password'] != user[0].password:
				result['status'] = False
				result['errors'].append("incorrect password")	
			else:
				result['status']= True
				result['user'] = user
		return result




class Users(models.Model):
	f_name = models.CharField(max_length = 100)
	l_name = models.CharField(max_length = 100)
	email = models.CharField(max_length = 100)
	password = models.CharField(max_length = 100)
	created_at = models.DateTimeField(auto_now_add = True)
	updated_at = models.DateTimeField(auto_now = True)
	
	objects = UserManager()
