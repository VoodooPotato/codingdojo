# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import messages
from django.shortcuts import render, redirect
from models import Users

def index(request):
	return render(request,'first_app/index.html')

def register(request):
	result = Users.objects.registerVal(request.POST)
	if(result['status'] == False):
		for error in result['errors']:
			messages.error(request,error)
	else:
		user = Users.objects.create(f_name = request.POST['f_name'],
			l_name = request.POST['l_name'],
			email = request.POST['email'],
			password = request.POST['password'],
			)
		messages.success(request,"User has been created. Please Log in")
	return redirect('/')

def login(request):
	results = Users.objects.loginVal(request.POST)
	if(results['status'] == False):
		for error in results['errors']:
			messages.error(request,error)
	else:
		messages.success(request,'You are logged in')
	return redirect('/')
