# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render,redirect
from random import randrange

def root(request):
	try:
		print request.session['account']
		return render(request,"ninjaGold/index.html", {'gold' : request.session['gold']}, {'account' : request.session['account']})
	except:
		request.session['gold'] = 0
		request.session['account'] = ''
		return render(request,"ninjaGold/index.html", {'gold' : request.session['gold']}, {'account' : request.session['account']})
def index(request):
	if request.POST['action'] == 'farm':
		earn = randrange(10,21)
		request.session['gold'] += earn
		request.session['account'] += "Earned {} from the farm\n".format(earn) 
	elif request.POST['action'] == 'cave':
		earn = randrange(5,11)
		request.session['gold'] += earn
		request.session['account'] += "Earned {} from the cave\n".format(earn)
	elif request.POST['action'] == 'house':
		earn = randrange(2,6)
		request.session['gold'] += earn
		request.session['account'] += "Earned {} from the house\n".format(earn)
	else:
		earn = randrange(0,51)
		if(randrange(0,2)):
			request.session['gold'] += earn
			request.session['account'] += "Earned {} from the casino\n".format(earn)
		else:
			request.session['gold'] -= earn
			request.session['account'] += "Losed {} from the casino\n".format(earn)
	return  redirect('/')

