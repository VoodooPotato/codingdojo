class product(object):
	"""docstring for product"""
	def __init__(self, price, name,weight,brand,cost,status):
		self.price = price
		self.name = name
		self.weight = weight 
		self.brand = brand
		self.cost = cost
		self.status = status
	def sell(self):
		self.status = "Sold"
		return self
	def add_tax(self,tax):
		return self.price * tax  + self.price
	def Return(self,reason):
		if(reason == "defective"):
			self.price = 0
			self.status = reason 
		elif(reason == "like new"):
			self.status = "For sale"
		else:
			self.status = "used"
			self.price = self.price - self.price*1/5
		return self 
	def Display_info(self):
		print"Price: {}$\nName: {}\nWeight: {}\nBrand: {}\nCost: {}$\nStatus: {}".format(self.price,self.name,self.weight,self.brand,self.cost,self.status)
		return self
apple = product(10,'apple',5,"state Farm",10,"for sell")
apple.Display_info().Return("defective").Display_info()