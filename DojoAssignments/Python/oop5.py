class MathDojo(object):
	"""docstring for MathDojo"""
	def __init__(self):
		self.total = 0
	def add(self,arg1,arg2):
		if(type(arg1) == tuple or type(arg1) == list):
			self.total += sum(arg1)
		else:
			self.total += arg1
		if(type(arg2) == tuple or type(arg2) == list):
			self.total += sum(arg2)
		else:
			self.total += arg2
		return self
	def result(self):
		print self.total
		return self
		