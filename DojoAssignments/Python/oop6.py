class Call(object):
	"""docstring for Call"""
	def __init__(self, Id,name,number,time,reason):
		self.id = Id
		self.name = name
		self.number = number
		self.time = time
		self.reason = reason
	def display(self):
		print 'ID: {}\ncaller: {}\nnumber: {}\ntime: {}\nreason: {}'.format(self.id,self.name,self.number,self.time,self.reason)
		return self

class callCenter(object):
	"""docstring for callCenter"""
	def __init__(self, callers):
		self.callers = callers
		self.size = len(callers)
	def add(self,caller):
		self.callers.append(caller)
		return self
	def remove(self,number=-1):
		if(number == -1):
			self.callers = self.callers[1:]
		else:
			for caller in self.callers:
				if(number == caller.number):
					self.callers.remove(caller)
					break
			else:
				print"number not found!"
		return self
	def info(self):
		for caller in self.callers:
			print 'ID: {}\ncaller: {}\nnumber: {}\ntime: {}\nreason: {}'.format(caller.id,caller.name,caller.number,caller.time,caller.reason)
		print self.size
		return self
	def sort(self):
		self.callers=sorted(self.callers, key=lambda caller: caller.time) 
		# note to self 
		# lambda denotes anonymous functions.
		# this is equivalent to // def function(caller):
		#								return caller.time
		# this concept is similar to JavaScript function within function
		return self


John = Call(01,"John",1,0,"hello")
Ben = Call(01,"Ben",1,9,"hello")
Steven = Call(01,"Steven",1,2,"hello")
Collen = Call(01,"Collen",1,8,"hello")
Stewie = Call(01,"Stewie",1,5,"hello")
center = callCenter([John,Ben,Steven,Collen,Stewie])
center.sort().info()

	
		