class Patient(object):
	"""docstring for Patient"""
	def __init__(self, Id,name,allergies,bed='none'):
		self.id = Id
		self.name = name
		self.allergies = allergies
		self.bed = bed

class Hospital(object):
	"""docstring for Hospital"""
	def __init__(self,name,capacity,patients=[]):
		self.patients=patients
		self.name = name
		self.capacity = capacity
	def admit(self,patient):
		if(len(self.patients) == self.capacity):
			print "Hosipital is full"
		else:
			self.patients.append(patient)
			patient.bed = len(self.patients)
			print "Admission complete"
		return self
	def Discharge(self,person):
		for patient in self.patients:
			if (patient.id == person.id):
				self.patients.remove(person)
				person.bed = 'none'
				print "patient has been removed"
				break
		else:
			print "There is no such person here"
		return self
	def list(self):
		for patient in self.patients:
			print'id = {} name = {} allergies = {} bed = {} '.format(patient.id, patient.name,patient.allergies,patient.bed)
		return self

John = Patient(1,"John","bugs")
Jessie  = Patient(2,"Jessie","Bugs")
uw = Hospital('University of Washington',10)
uw.admit(John).admit(Jessie).list().Discharge(Jessie).list()