# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class products(models.Model):
	description = models.TextField(max_length = 400)
	weight = models.IntegerField()
	price = models.IntegerField()
	cost = models.IntegerField()
	category = models.CharField(max_length = 10)
	in_print = models.BooleanField()
