function name(matrix) {
	console.log("Students");
	for(var pupil in matrix.Students){
		var first_name = matrix.Students[pupil].first_name.toUpperCase();
		var last_name = matrix.Students[pupil].last_name.toUpperCase();
		console.log(pupil + " - " + first_name + " " + last_name + " - " + (first_name.length + last_name.length));
	}
	console.log("\nteacher")
	for(var teacher in matrix.Instructors){
		var first_name = matrix.Instructors[teacher].first_name.toUpperCase();
		var last_name = matrix.Instructors[teacher].last_name.toUpperCase();
		console.log(teacher + " - " + first_name + " " + last_name + " - " + (first_name.length + last_name.length));
	}
}

name( {'Students': [ 
     {first_name:  'Michael', last_name : 'Jordan'},
     {first_name : 'John', last_name : 'Rosales'},
     {first_name : 'Mark', last_name : 'Guillen'},
     {first_name : 'KB', last_name : 'Tonel'}
  ],
 'Instructors': [
     {first_name : 'Michael', last_name : 'Choi'},
     {first_name : 'Martin', last_name : 'Puryear'}
  ]
 });