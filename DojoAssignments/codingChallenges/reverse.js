function reverse(str) {
	var reversed = '';
	for(var i = str.length -1; i >= 0;i--){
		reversed += str[i];
	}
	return reversed;
}
function param(str){
	var open = 0;
	var close = 0;
	for(var i = 0; i < str.length;i++){
		if(str[i] == '('){
			open +=1;
		}else if (str[i] == ')'){
			close +=1;
		}
	}
	if(open == close){
		return true;
	}
	return false;
}
console.log(reverse('hello'));
console.log(param('()()(()'))